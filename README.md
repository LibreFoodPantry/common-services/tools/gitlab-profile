# Tools

The LFP Pipeline provides a framework for projects to build a GitLab
Pipeline for their project.

See the
[documentation](https://gitlab.com/LibreFoodPantry/common-services/tools/documentation/-/blob/main/README.md).
